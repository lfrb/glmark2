/*
 * Copyright © 2012 Linaro Limited
 * Copyright © 2013 Canonical Ltd
 *
 * This file is part of the glmark2 OpenGL (ES) 2.0 benchmark.
 *
 * glmark2 is free software: you can redistribute it and/or modify it under the
 * terms of the GNU General Public License as published by the Free Software
 * Foundation, either version 3 of the License, or (at your option) any later
 * version.
 *
 * glmark2 is distributed in the hope that it will be useful, but WITHOUT ANY
 * WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE.  See the GNU General Public License for more
 * details.
 *
 * You should have received a copy of the GNU General Public License along with
 * glmark2.  If not, see <http://www.gnu.org/licenses/>.
 *
 * Authors:
 *  Simon Que 
 *  Jesse Barker
 *  Alexandros Frantzis
 */
#include "native-state-drm.h"
#include "log.h"

#include <fcntl.h>
#include <libudev.h>
#include <cstring>
#include <string>

#include <drm_fourcc.h>

#ifndef DRM_CLIENT_CAP_UNIVERSAL_PLANES
#define DRM_CLIENT_CAP_UNIVERSAL_PLANES 2
#endif

static inline uint32_t *
formats_ptr(struct drm_format_modifier_blob *blob)
{
	return (uint32_t *)(((char *)blob) + blob->formats_offset);
}

static inline struct drm_format_modifier *
modifiers_ptr(struct drm_format_modifier_blob *blob)
{
	return (struct drm_format_modifier *)(((char *)blob) + blob->modifiers_offset);
}

/******************
 * Public methods *
 ******************/

bool
NativeStateDRM::init_display()
{
    if (!dev_)
        init();

    Log::debug("NativeStateDRM::init_display()\n");
    return (dev_ != 0);
}

void*
NativeStateDRM::display()
{
    return static_cast<void*>(dev_);
}

bool
NativeStateDRM::create_window(WindowProperties const& /*properties*/)
{
    if (!dev_) {
        Log::error("Error: DRM device has not been initialized!\n");
        return false;
    }

    return true;
}

void*
NativeStateDRM::window(WindowProperties& properties)
{
    properties = WindowProperties(mode_->hdisplay,
                                  mode_->vdisplay,
                                  true, 0);
    return static_cast<void*>(surface_);
}

void
NativeStateDRM::visible(bool /*visible*/)
{
}

bool
NativeStateDRM::should_quit()
{
    return should_quit_;
}

void
NativeStateDRM::flip()
{
    gbm_bo* next = gbm_surface_lock_front_buffer(surface_);
    fb_ = fb_get_from_bo(next);
    unsigned int waiting(1);

    if (!crtc_set_) {
        int status = drmModeSetCrtc(fd_, encoder_->crtc_id, fb_->fb_id, 0, 0,
                                    &connector_->connector_id, 1, mode_);
        if (status >= 0) {
            crtc_set_ = true;
            bo_ = next;
        }
        else {
            Log::error("Failed to set crtc: %d\n", status);
        }
        return;
    }

#if 0
    int status = drmModePageFlip(fd_, encoder_->crtc_id, fb_->fb_id,
                                 DRM_MODE_PAGE_FLIP_EVENT | DRM_MODE_PAGE_FLIP_ASYNC,
                                 &waiting);
#else
    Log::debug("Atomic commit for NativeStateDRM %p\n", this);
    int status = atomic_commit(fb_->fb_id, &waiting,
                               DRM_MODE_ATOMIC_NONBLOCK | DRM_MODE_PAGE_FLIP_EVENT);
#endif

    if (status < 0) {
        Log::error("Failed to enqueue page flip: %d\n", status);
        return;
    }

    fd_set fds;
    FD_ZERO(&fds);
    FD_SET(fd_, &fds);
    drmEventContext evCtx;
    memset(&evCtx, 0, sizeof(evCtx));
    evCtx.version = 2;
    evCtx.page_flip_handler = page_flip_handler;

    while (waiting) {
        status = select(fd_ + 1, &fds, 0, 0, 0);
        if (status < 0) {
            // Most of the time, select() will return an error because the
            // user pressed Ctrl-C.  So, only print out a message in debug
            // mode, and just check for the likely condition and release
            // the current buffer object before getting out.
            Log::debug("Error in select\n");
            if (should_quit()) {
                gbm_surface_release_buffer(surface_, bo_);
                bo_ = next;
            }
            return;
        }
        drmHandleEvent(fd_, &evCtx);
    }

    gbm_surface_release_buffer(surface_, bo_);
    bo_ = next;
}

/*******************
 * Private methods *
 *******************/

/* Simple helpers */

inline static bool valid_fd(int fd)
{
    return fd >= 0;
}

inline static bool valid_drm_node_path(std::string const& provided_node_path)
{
    return !provided_node_path.empty();
}

inline static bool invalid_drm_node_path(std::string const& provided_node_path)
{
    return !(valid_drm_node_path(provided_node_path));
}

/* Udev methods */
// Udev detection functions
#define UDEV_TEST_FUNC_SIGNATURE(udev_identifier, device_identifier, syspath_identifier) \
    struct udev * __restrict const udev_identifier, \
    struct udev_device * __restrict const device_identifier, \
    char const * __restrict syspath_identifier

/* Omitting the parameter names is kind of ugly but is the only way
 * to force G++ to forget about the unused parameters.
 * Having big warnings during the compilation isn't very nice.
 *
 * These functions will be used as function pointers and should have
 * the same signature to avoid weird stack related issues.
 *
 * Another way to deal with that issue will be to mark unused parameters
 * with __attribute__((unused))
 */
static bool udev_drm_test_virtual(
    UDEV_TEST_FUNC_SIGNATURE(,,tested_node_syspath))
{
    return strstr(tested_node_syspath, "virtual") != NULL;
}

static bool udev_drm_test_not_virtual(
    UDEV_TEST_FUNC_SIGNATURE(udev, current_device, tested_node_syspath))
{
    return !udev_drm_test_virtual(udev,
                                  current_device,
                                  tested_node_syspath);
}

static bool
udev_drm_test_primary_gpu(UDEV_TEST_FUNC_SIGNATURE(, current_device,))
{
    bool is_main_gpu = false;

    auto const drm_node_parent = udev_device_get_parent(current_device);

    /* While tempting, using udev_device_unref will generate issues
     * when unreferencing the child in udev_get_node_that_pass_in_enum
     *
     * udev_device_unref WILL unreference the parent, so avoid doing
     * that here.
     *
     * ( See udev sources : src/libudev/libudev-device.c )
     */
    if (drm_node_parent != NULL) {
        is_main_gpu =
            (udev_device_get_sysattr_value(drm_node_parent, "boot_vga")
             != NULL);
    }

    return is_main_gpu;
}

static std::string
udev_get_node_that_pass_in_enum(
    struct udev * __restrict const udev,
    struct udev_enumerate * __restrict const dev_enum,
    bool (* check_function)(UDEV_TEST_FUNC_SIGNATURE(,,)))
{
    std::string result;

    auto current_element = udev_enumerate_get_list_entry(dev_enum);

    while (current_element && result.empty()) {
        char const * __restrict current_element_sys_path =
            udev_list_entry_get_name(current_element);

        if (current_element_sys_path) {
            struct udev_device * current_device =
                udev_device_new_from_syspath(udev,
                                             current_element_sys_path);
            auto check_passed = check_function(
                udev, current_device, current_element_sys_path);

            if (check_passed) {
                const char * device_node_path =
                    udev_device_get_devnode(current_device);

                if (device_node_path) {
                    result = device_node_path;
                }

            }

            udev_device_unref(current_device);
        }

        current_element = udev_list_entry_get_next(current_element);
    }

    return result;
}

/* Inspired by KWin detection mechanism */
/* And yet KWin got it wrong too, it seems.
 * 1 - Looking for the primary GPU by checking the flag 'boot_vga'
 *     won't get you far with some embedded chipsets, like Rockchip.
 * 2 - Looking for a GPU plugged in PCI will fail on various embedded
 *     devices !
 * 3 - Looking for a render node is not guaranteed to work on some
 *     poorly maintained DRM drivers, which plague some embedded
 *     devices too...
 *
 * So, we won't play too smart here.
 * - We first check for a primary GPU plugged in PCI with the 'boot_vga'
 *   attribute, to take care of Desktop users using multiple GPU.
 * - Then, we just check for a DRM node that is not virtual
 * - At least, we use the first virtual node we get, if we didn't find
 *   anything yet.
 * This should take care of almost every potential use case.
 *
 * The remaining ones will be dealt with an additional option to
 * specify the DRM dev node manually.
 */
static std::string udev_main_gpu_drm_node_path()
{
    Log::debug("Using Udev to detect the right DRM node to use\n");
    auto udev = udev_new();
    auto dev_enumeration = udev_enumerate_new(udev);

    udev_enumerate_add_match_subsystem(dev_enumeration, "drm");
    udev_enumerate_add_match_sysname(dev_enumeration, "card[0-9]*");
    udev_enumerate_scan_devices(dev_enumeration);

    Log::debug("Looking for the main GPU DRM node...\n");
    std::string node_path = udev_get_node_that_pass_in_enum(
        udev, dev_enumeration, udev_drm_test_primary_gpu);

    if (invalid_drm_node_path(node_path)) {
        Log::debug("Not found!\n");
        Log::debug("Looking for a concrete GPU DRM node...\n");
        node_path = udev_get_node_that_pass_in_enum(
            udev, dev_enumeration, udev_drm_test_not_virtual);
    }
    if (invalid_drm_node_path(node_path)) {
        Log::debug("Not found!?\n");
        Log::debug("Looking for a virtual GPU DRM node...\n");
        node_path = udev_get_node_that_pass_in_enum(
            udev, dev_enumeration, udev_drm_test_virtual);
    }
    if (invalid_drm_node_path(node_path)) {
        Log::debug("Not found.\n");
        Log::debug("Cannot find a single DRM node using UDEV...\n");
    }

    if (valid_drm_node_path(node_path)) {
        Log::debug("Success!\n");
    }

    udev_enumerate_unref(dev_enumeration);
    udev_unref(udev);

    return node_path;
}

static int open_using_udev_scan()
{
    auto dev_path = udev_main_gpu_drm_node_path();

    int fd = -1;
    if (valid_drm_node_path(dev_path)) {
        Log::debug("Trying to use the DRM node %s\n", dev_path.c_str());
        fd = open(dev_path.c_str(), O_RDWR);
    }
    else {
        Log::error("Can't determine the main graphic card "
                   "DRM device node\n");
    }

    if (!valid_fd(fd)) {
        // %m is GLIBC specific... Maybe use strerror here...
        Log::error("Tried to use '%s' but failed.\nReason : %m",
                   dev_path.c_str());
    }
    else
        Log::debug("Success!\n");

    return fd;
}

/* -- End of Udev helpers -- */

/*
 * This method is there to take care of cases that would not be handled
 * by open_using_udev_scan. This should not happen.
 *
 * If your driver defines a /dev/dri/cardX node and open_using_udev_scan
 * were not able to detect it, you should probably file an issue.
 *
 */
static int open_using_module_checking()
{
    static const char* drm_modules[] = {
        "i915",
        "imx-drm",
        "nouveau",
        "radeon",
        "vmgfx",
        "omapdrm",
        "exynos",
        "pl111",
        "vc4",
    };

    int fd = -1;
    unsigned int num_modules(sizeof(drm_modules)/sizeof(drm_modules[0]));
    for (unsigned int m = 0; m < num_modules; m++) {
        fd = drmOpen(drm_modules[m], 0);
        if (fd < 0) {
            Log::debug("Failed to open DRM module '%s'\n", drm_modules[m]);
            continue;
        }
        Log::debug("Opened DRM module '%s'\n", drm_modules[m]);
        break;
    }

    return fd;
}

void
NativeStateDRM::fb_destroy_callback(gbm_bo* bo, void* data)
{
    DRMFBState* fb = reinterpret_cast<DRMFBState*>(data);
    if (fb && fb->fb_id) {
        drmModeRmFB(fb->fd, fb->fb_id);
    }
    delete fb;
    gbm_device* dev = gbm_bo_get_device(bo);
    Log::debug("Got GBM device handle %p from buffer object\n", dev);
}

NativeStateDRM::DRMFBState*
NativeStateDRM::fb_get_from_bo(gbm_bo* bo)
{
    DRMFBState* fb = reinterpret_cast<DRMFBState*>(gbm_bo_get_user_data(bo));
    if (fb) {
        return fb;
    }

    unsigned int width = gbm_bo_get_width(bo);
    unsigned int height = gbm_bo_get_height(bo);
    unsigned int strides[4] = { 0 };
    unsigned int offsets[4] = { 0 };
    unsigned int handles[4] = { 0 };
    uint64_t modifiers[4]   = { 0 };
    unsigned int fb_id(0);
    int status;

    for (int i = 0; i < gbm_bo_get_plane_count(bo); i++) {
        modifiers[i] = gbm_bo_get_modifier(bo);
        strides[i] = gbm_bo_get_stride_for_plane(bo, i);
        handles[i] = gbm_bo_get_handle_for_plane(bo, i).u32;
        offsets[i] = gbm_bo_get_offset(bo, i);
    }

    if (modifiers[0] != DRM_FORMAT_MOD_INVALID) {
        Log::info("Using modifier %llx\n", modifiers[0]);
        status = drmModeAddFB2WithModifiers(fd_, width, height, DRM_FORMAT_XRGB8888,
                                            handles, strides, offsets,
                                            modifiers, &fb_id,
                                            DRM_MODE_FB_MODIFIERS);
    } else {
        status = drmModeAddFB(fd_, width, height, 24, 32, strides[0], handles[0], &fb_id);
    }

    if (status < 0) {
        Log::error("Failed to create FB: %d\n", status);
        return 0;
    }

    fb = new DRMFBState();
    fb->fd = fd_;
    fb->bo = bo;
    fb->fb_id = fb_id;

    gbm_bo_set_user_data(bo, fb, fb_destroy_callback);
    return fb;
}

int
NativeStateDRM::add_connector_property(drmModeAtomicReq *req,
                                       const char *name, uint64_t value)
{
    unsigned int i;
    int prop_id = 0;

    if (!connector_props_) {
        Log::error("no connector props\n");
        return -EINVAL;
    }

    for (i = 0 ; i < connector_props_->props->count_props ; i++) {
        if (strcmp(connector_props_->info[i]->name, name) == 0) {
            prop_id = connector_props_->info[i]->prop_id;
            break;
        }
    }

    if (prop_id < 0) {
        Log::error("no connector property: %s\n", name);
        return -EINVAL;
    }

    return drmModeAtomicAddProperty(req, connector_->connector_id,
            prop_id, value);
}

int
NativeStateDRM::add_crtc_property(drmModeAtomicReq *req,
                                  const char *name, uint64_t value)
{
    unsigned int i;
    int prop_id = -1;

    if (!crtc_props_) {
        Log::error("no crtc props\n");
        return -EINVAL;
    }

    for (i = 0 ; i < crtc_props_->props->count_props ; i++) {
        if (strcmp(crtc_props_->info[i]->name, name) == 0) {
            prop_id = crtc_props_->info[i]->prop_id;
            break;
        }
    }

    if (prop_id < 0) {
        Log::error("no crtc property: %s\n", name);
        return -EINVAL;
    }

    return drmModeAtomicAddProperty(req, encoder_->crtc_id, prop_id, value);
}

int
NativeStateDRM::add_plane_property(drmModeAtomicReq *req,
                                   const char *name, uint64_t value)
{
    unsigned int i;
    int prop_id = -1;

    if (!plane_props_) {
        Log::error("no plane props\n");
        return -EINVAL;
    }

    for (i = 0; i < plane_props_->props->count_props; i++) {
        if (strcmp(plane_props_->info[i]->name, name) == 0) {
            prop_id = plane_props_->info[i]->prop_id;
            break;
        }
    }

    if (prop_id < 0) {
        Log::error("no plane property: %s\n", name);
        return -EINVAL;
    }

    return drmModeAtomicAddProperty(req, plane_->plane_id, prop_id, value);
}

int
NativeStateDRM::atomic_commit(uint32_t fb_id, void *data, uint32_t flags)
{
    drmModeAtomicReq *req;
    uint32_t blob_id;
    int ret;

    req = drmModeAtomicAlloc();

    if (flags & DRM_MODE_ATOMIC_ALLOW_MODESET) {
        if (add_connector_property(req, "CRTC_ID", encoder_->crtc_id) < 0)
            return -1;

        if (drmModeCreatePropertyBlob(fd_, mode_, sizeof(*mode_),
                                      &blob_id) != 0)
            return -1;

        if (add_crtc_property(req, "MODE_ID", blob_id) < 0)
            return -1;

        if (add_crtc_property(req, "ACTIVE", 1) < 0)
            return -1;
    }

    add_plane_property(req, "FB_ID", fb_id);
    add_plane_property(req, "CRTC_ID", encoder_->crtc_id);
    add_plane_property(req, "SRC_X", 0);
    add_plane_property(req, "SRC_Y", 0);
    add_plane_property(req, "SRC_W", mode_->hdisplay << 16);
    add_plane_property(req, "SRC_H", mode_->vdisplay << 16);
    add_plane_property(req, "CRTC_X", 0);
    add_plane_property(req, "CRTC_Y", 0);
    add_plane_property(req, "CRTC_W", mode_->hdisplay);
    add_plane_property(req, "CRTC_H", mode_->vdisplay);

    ret = drmModeAtomicCommit(fd_, req, flags, data);
    if (ret)
        goto out;

out:
    drmModeAtomicFree(req);
    return ret;
}

#if 0
bool
NativeStateDRM::populate_format_modifiers(const drmModePlane *kplane,
			                                 uint32_t blob_id)
{
    unsigned i, j;
    drmModePropertyBlobRes *blob;
    struct drm_format_modifier_blob *fmt_mod_blob;
    uint32_t *blob_formats;
    struct drm_format_modifier *blob_modifiers;

    blob = drmModeGetPropertyBlob(fd_, blob_id);
    if (!blob)
        return false;

    fmt_mod_blob = blob->data;
    blob_formats = formats_ptr(fmt_mod_blob);
    blob_modifiers = modifiers_ptr(fmt_mod_blob);

    for (i = 0; i < fmt_mod_blob->count_formats; i++) {
        uint32_t count_modifiers = 0;
        uint64_t *modifiers = NULL;

        for (j = 0; j < fmt_mod_blob->count_modifiers; j++) {
            struct drm_format_modifier *mod = &blob_modifiers[j];

            if ((i < mod->offset) || (i > mod->offset + 63))
                continue;
            if (!(mod->formats & (1 << (i - mod->offset))))
                continue;

            modifiers = realloc(modifiers, (count_modifiers + 1) * sizeof(modifiers[0]));
            if (!modifiers) {
                kkkkkkkkkkkkdrmModeFreePropertyBlob(blob);
                return false;
            }
            modifiers[count_modifiers++] = mod->modifier;
        }

        formats_[i].format = blob_formats[i];
        formats_[i].modifiers = modifiers;
        formats_[i].count_modifiers = count_modifiers;
    }

    drmModeFreePropertyBlob(blob);

    return true;
}
#endif

#if 0
bool
NativeStateDRM::init_planes(void)
{
    drmModePlaneRes *kplane_res;
    drmModePlane *kplane;
	 drmModeObjectProperties *props;
    struct drm_plane *drm_plane;
    uint32_t i;

    kplane_res = drmModeGetPlaneResources(fd_);
    if (!kplane_res)
        return false;

    for (i = 0; i < kplane_res->count_planes; i++) {
        uint32_t num_formats;
		  uint32_t blob_id;

        kplane = drmModeGetPlane(fd_, kplane_res->planes[i]);
        if (!kplane)
            continue;

	     num_formats_ = (kplane) ? kplane->count_formats : 1;
	     formats_ = zalloc(sizeof(struct ) * num_formats_);

		//plane->possible_crtcs = kplane->possible_crtcs;
		//plane->plane_id = kplane->plane_id;

		props = drmModeObjectGetProperties(b->drm.fd, kplane->plane_id,
						   DRM_MODE_OBJECT_PLANE);
		if (!props) {
			weston_log("couldn't get plane properties\n");
			goto err;
		}
		drm_property_info_populate(b, plane_props, plane_props_,
					   WDRM_PLANE__COUNT, props);
		plane->type =
			drm_property_get_value(&plane_props_[WDRM_PLANE_TYPE],
					       props,
					       WDRM_PLANE_TYPE__COUNT);

		blob_id = drm_property_get_value(&plane->props[WDRM_PLANE_IN_FORMATS],
					         props,
					         0);
      if (blob_id)
          populate_format_modifiers(plane, kplane, blob_id);
    }

    drmModeFreePlaneResources(kplane_res);

    return true;
}
#endif

bool
NativeStateDRM::init_gbm()
{
    uint64_t modifiers[6] = {
        I915_FORMAT_MOD_X_TILED,
        I915_FORMAT_MOD_Y_TILED,
        I915_FORMAT_MOD_Y_TILED_CCS,
        I915_FORMAT_MOD_Yf_TILED_CCS,
        DRM_FORMAT_MOD_LINEAR,
        DRM_FORMAT_MOD_INVALID
    };
    dev_ = gbm_create_device(fd_);
    if (!dev_) {
        Log::error("Failed to create GBM device\n");
        return false;
    }

    if (true) {
        surface_ = gbm_surface_create_with_modifiers(dev_,
                                                     mode_->hdisplay,
                                                     mode_->vdisplay,
                                                     GBM_FORMAT_XRGB8888,
                                                     modifiers, 6);
    } else {
        surface_ = gbm_surface_create(dev_, mode_->hdisplay, mode_->vdisplay,
                                      GBM_FORMAT_XRGB8888,
                                      GBM_BO_USE_SCANOUT | GBM_BO_USE_RENDERING);
    }

    if (!surface_) {
        Log::error("Failed to create GBM surface\n");
        return false;
    }

    return true;
}

bool
NativeStateDRM::init()
{
    // TODO: The user should be able to define *exactly* which device
    //       node to open and the program should try to open only
    //       this node, in order to take care of unknown use cases.
    drmModePlaneResPtr plane_resources;
    int fd = open_using_udev_scan();

    Log::debug("Initializing DRM\n");

    if (!valid_fd(fd)) {
        fd = open_using_module_checking();
    }

    if (!valid_fd(fd)) {
        Log::error("Failed to find a suitable DRM device\n");
        return false;
    }

    fd_ = fd;

    resources_ = drmModeGetResources(fd);
    if (!resources_) {
        Log::error("drmModeGetResources failed\n");
        return false;
    }

    drmSetClientCap(fd, DRM_CLIENT_CAP_ATOMIC, 1);
    drmSetClientCap(fd, DRM_CLIENT_CAP_UNIVERSAL_PLANES, 1);

    // Find a connected connector
    for (int c = 0; c < resources_->count_connectors; c++) {
        connector_ = drmModeGetConnector(fd, resources_->connectors[c]);
        if (DRM_MODE_CONNECTED == connector_->connection) {
            break;
        }
        drmModeFreeConnector(connector_);
        connector_ = 0;
    }

    if (!connector_) {
        Log::error("Failed to find a suitable connector\n");
        return false;
    }

    // Find the best resolution (we will always operate full-screen).
    unsigned int bestArea(0);
    for (int m = 0; m < connector_->count_modes; m++) {
        drmModeModeInfo* curMode = &connector_->modes[m];
        unsigned int curArea = curMode->hdisplay * curMode->vdisplay;
        if (curArea > bestArea) {
            mode_ = curMode;
            bestArea = curArea;
        }
    }

    if (!mode_) {
        Log::error("Failed to find a suitable mode\n");
        return false;
    }

    // Find a suitable encoder
    for (int e = 0; e < resources_->count_encoders; e++) {
        bool found = false;
        encoder_ = drmModeGetEncoder(fd_, resources_->encoders[e]);
        for (int ce = 0; e < connector_->count_encoders; ce++) {
            if (encoder_ && encoder_->encoder_id == connector_->encoders[ce]) {
                found = true;
                break;
            }
        }
        if (found)
            break;
        drmModeFreeEncoder(encoder_);
        encoder_ = 0;
    }

    if (!encoder_) {
        Log::error("Failed to find a suitable encoder\n");
        return false;
    }

    // Find the primary plane
    plane_resources = drmModeGetPlaneResources(fd);
    if (!plane_resources)
        return false;

    for (unsigned int i = 0; i < plane_resources->count_planes; i++) {
        uint32_t id = plane_resources->planes[i];
        drmModePlanePtr plane = drmModeGetPlane(fd, id);

        if (!plane)
            continue;

        // XXX Check possible_crtcs
        drmModeObjectPropertiesPtr props =
            drmModeObjectGetProperties(fd, id, DRM_MODE_OBJECT_PLANE);

        for (unsigned int j = 0; j < props->count_props; j++) {
            drmModePropertyPtr p =
                drmModeGetProperty(fd, props->props[j]);

            if ((strcmp(p->name, "type") == 0) &&
                    (props->prop_values[j] == DRM_PLANE_TYPE_PRIMARY)) {
                plane_ = plane;
                break;
            }
            drmModeFreeProperty(p);
        }

        drmModeFreeObjectProperties(props);
        if (plane_)
            break;
        else
            drmModeFreePlane(plane);
    }
    drmModeFreePlaneResources(plane_resources);

    if (!init_gbm()) {
        return false;
    }

    crtc_ = drmModeGetCrtc(fd_, encoder_->crtc_id);
    if (!crtc_) {
        // if there is no current CRTC, make sure to attach a suitable one
        for (int c = 0; c < resources_->count_crtcs; c++) {
            if (encoder_->possible_crtcs & (1 << c)) {
                encoder_->crtc_id = resources_->crtcs[c];
                break;
            }
        }
    }

#define get_properties(p, TYPE, id) do {    		        \
    p = new DRMObjectProps;                                     \
    p->props = drmModeObjectGetProperties(fd,                   \
            id, DRM_MODE_OBJECT_##TYPE);			\
    if (!p->props)				                \
        return false;						\
    p->info = new drmModePropertyRes*[p->props->count_props];	\
    for (uint32_t i = 0; i < p->props->count_props; i++) {	\
        p->info[i] = drmModeGetProperty(fd, p->props->props[i]);\
        Log::debug("Property %s\n", p->info[i]->name);          \
    }								\
} while (0)

    Log::debug("Getting properties for NativeStateDRM %p\n", this);
    get_properties(plane_props_, PLANE, plane_->plane_id);
    Log::debug("Plane %i has %i properties\n", plane_->plane_id, plane_props_->props->count_props);
    get_properties(crtc_props_, CRTC, encoder_->crtc_id);
    get_properties(connector_props_, CONNECTOR, connector_->connector_id);

    signal(SIGINT, &NativeStateDRM::quit_handler);

    return true;
}

volatile std::sig_atomic_t NativeStateDRM::should_quit_(false);

void
NativeStateDRM::quit_handler(int /*signo*/)
{
    should_quit_ = true;
}

void
NativeStateDRM::page_flip_handler(int/*  fd */, unsigned int /* frame */, unsigned int /* sec */, unsigned int /* usec */, void* data)
{
    unsigned int* waiting = reinterpret_cast<unsigned int*>(data);
    *waiting = 0;
}

void
NativeStateDRM::cleanup()
{
    // Restore CRTC state if necessary
    if (crtc_) {
        int status = drmModeSetCrtc(fd_, crtc_->crtc_id, crtc_->buffer_id,
                                    crtc_->x, crtc_->y, &connector_->connector_id,
                                    1, &crtc_->mode);
        if (status < 0) {
            Log::error("Failed to restore original CRTC: %d\n", status);
        }
        drmModeFreeCrtc(crtc_);
        crtc_ = 0;
    }
    if (surface_) {
        gbm_surface_destroy(surface_);
        surface_ = 0;
    }
    if (dev_) {
        gbm_device_destroy(dev_);
        dev_ = 0;
    }
    if (connector_) {
        drmModeFreeConnector(connector_);
        connector_ = 0;
    }
    if (encoder_) {
        drmModeFreeEncoder(encoder_);
        encoder_ = 0;
    }
    if (resources_) {
        drmModeFreeResources(resources_);
        resources_ = 0;
    }
    if (fd_ > 0) {
        drmClose(fd_);
    }
    fd_ = 0;
    mode_ = 0;
}
